
# Garden Guardian 


Welcome to Garden Guardian Web Application! 🌱🌻🌿

![Alt text](ScreenShot/CommunityPage.png)

## Introduction

The Garden Guardian Web Application is a place where gardening enthusiasts can come together to showcase their creativity, learn from each other, and connect with other users. With our clean interface and robust features, you can easily create and maintain your own virtual garden, discover new plants, find reliable retailers, join exciting community events, and explore the beautiful gardens of others.

## Features

- **Create Virtual Gardens:** Design and cultivate your dream garden virtually by adding a variety of plants. Arrange and design your garden layout to your heart's content.

- **Plant Database:** Access a comprehensive database of plants to choose from. Each plant comes with detailed information to help you make informed decisions.

- **Retailer Recommendations:** Find trusted retailers near you or online, making it convenient to purchase the plants you desire.

- **Community Events:** Participate in engaging events hosted by the community. Learn new gardening techniques, exchange tips, and bond with fellow gardeners.

- **Explore User Gardens:** Get inspired by exploring the virtual gardens of other community members. Share your gardening journey and gain insights from fellow enthusiasts.

## Technologies Used

- **Server-side API:** Built using ASP.NET, providing robust and secure backend functionality.

- **Database Management:** Utilizes SQL Server Management Studio (SSMS) to manage the SQL database, ensuring data integrity and efficiency.

- **Web Application:** Developed with Vue.js, a progressive JavaScript framework, for seamless and responsive user experiences.

- **API Communication:** Relies on Axios for efficient handling of API promises, ensuring smooth communication between the frontend and backend.

## Getting Started

To get started with the Web Application, follow these steps:

1. Clone the repository: `git clone https://github.com/Mattkarpdev/Garden-Guardian.git`
2. Navigate to and run in IIS Express: `Garden-Guardian/capstone/dotnet/Capstone.sln` 
3. Navigate to the project directory: `cd capstone/vue`
4. Install dependencies: `npm install`
5. Start the development server: `npm run serve`
6. Access the application at: `http://localhost:8080`

## Usage

1. **Sign Up or Log In:** Create a new account or log in to your existing account.

2. **Create Your Virtual Garden:** Design and personalize your virtual garden by adding plants from the database.

3. **Explore Plants:** Browse the plant database, read detailed plant profiles, and choose the ones that match your preferences.

4. **Find Retailers:** Discover retailers where you can purchase the plants you've selected.

5. **Join Community Events:** Participate in community events to learn, share, and connect with fellow gardeners.

6. **Explore Gardens:** Take a stroll through other users' gardens for inspiration and ideas.

## Issues 

Please report any issues using github.

Known issues will be posted here, there are currently 0 known issues.

